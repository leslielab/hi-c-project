require(VennDiagram)
require(data.table)
require(stringr)


setwd('/cbio/cllab/nobackup/zamparol/revisions/annotated_data')

get_counts <- function(mychr){
  
  hicdc_sig_data = data.table(readRDS(file = paste0(mychr,"hicdc_significant_contacts_annotated.rds")))
  
  ### load Fit-Hi-C significant interaction data
  
  fit_hic_sig_data = data.table(readRDS(file = paste0(mychr,"fit_hic_significant_contacts_annotated.rds")))
  
  # join on bins, identify those sig in both, those sig only in Fit-Hi-C, those sig only in HiC-DC
  hicdc_sig_data[, binI := paste(chrI, startI, endI, sep="-")]
  hicdc_sig_data[, binJ := paste(chrJ, startJ, endJ, sep="-")]
  
  # left outer join on binI, binJ with fit_hic_sig_data, to identify differential significant bins.
  setkey(hicdc_sig_data, binI, binJ)
  setkey(fit_hic_sig_data, binI, binJ)
  
  fit_hic_sig_data <- fit_hic_sig_data[,.(binI, binJ, fithic_counts, D, fit_hic_pvalue, fit_hic_qvalue, annotation)]
  hicdc_sig_data <- hicdc_sig_data[,.(binI, binJ, counts, D, pvalue, qvalue, annotation)]
  
  # identify and count the set sizes
  sig_data = merge(hicdc_sig_data, fit_hic_sig_data, all.x = TRUE)
  sig_data[is.na(fit_hic_qvalue), origin := "only hicdc"]
  sig_data[!is.na(fit_hic_qvalue), origin := "both sig"]
  
  hicdc_count=sig_data[origin == "only hicdc",.N]
  both_count=sig_data[origin == "both sig",.N]
  fit_hic_count=fit_hic_sig_data[,.N] - both_count
  
  return(list(hicdc_only=hicdc_count,both=both_count,fit_hic_only=fit_hic_count))
}

chr_files = list.files(path = ".", pattern = "*fit_hic_significant_contacts_annotated.rds")
#this_chr <- str_extract(chr_file_name, "chr([\\d]+|X)")
my_chrs = unique(rapply(lapply(chr_files, str_extract, "chr([\\d]+|X)"), function(x) head(x,1)))

for (chr in my_chrs){
  my_counts = get_counts(chr)
  if (!exists("counts_df")){
    counts_df = data.frame(chrs=chr, only_hicdc=my_counts$hicdc_only, both=my_counts$both, only_fit_hic=my_counts$fit_hic_only)
  }
  else {
    counts_df = data.frame(rbind(counts_df, data.frame(chrs=chr, only_hicdc=my_counts$hicdc_only, both=my_counts$both, only_fit_hic=my_counts$fit_hic_only)))
  }
}

counts_dt = data.table(counts_df)

only_hicdc = counts_dt[,sum(only_hicdc)]
both = counts_dt[,sum(both)]
only_fithic = counts_dt[,sum(only_fit_hic)]

# sum the counts, construct the Venn Diagram
setwd("/cbio/cllab/nobackup/zamparol/revisions/")

pdf(file = "overlaps.pdf")
grid.newpage()
draw.pairwise.venn(only_hicdc, only_fithic, both, category = c("HiC-DC only", "Fit-Hi-C only"), lty = rep("blank",2), fill = c("light blue", "pink"), alpha = rep(0.5, 2), cat.pos = c(0,0), cat.dist = rep(0.025, 2))
dev.off()