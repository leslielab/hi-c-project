library("ggplot2")
library("data.table")
library("stringr")
library("cowplot")
library("dplyr")
library("ggthemes")
library("gridExtra")

### Try to make a filled proportion area plot of the genomically annotated contact types

# Load the .rds data files containing the enrichments
setwd('/Users/zamparol/projects/HiC-label/data/genomic_enrichments_with_diagonal')

# Read all the .rds files, merge into one data frame after labeling by chr
chr_files <- list.files(path = ".", pattern = "*chr[0-9|X]+.rds")
for (file in chr_files){
  df <- readRDS(file)
  chr_name <- str_extract(file, "chr([\\d]+|X)")
  df$chromosome <- chr_name
  if (!exists("all_chrs_df")){
    all_chrs_df <- df
  }
  else {
    all_chrs_df <- rbind(all_chrs_df,df)
  }
}

# combine counts together in one data table
all_chrs_dt <- data.table(all_chrs_df)

# For a summarized band of the data, produce the fisher.test values
do_ft <- function(sig_label,sig_notlabel,not_sig_label,not_sig_notlabel){
  ft <- fisher.test(matrix(c(sig_label,not_sig_label,sig_notlabel,not_sig_notlabel),nrow=2,ncol=2),alternative="greater")
  ft$p.value
}

# For all records of a given class, apply length(interaction_types) tests, of '<label> greater than <rest>'
# return the combined results of all length(interaction_types) tests
# N.B: first tried wilcoxon.test, now trying ks.test
do_enr_test <- function(class_df){
  interaction_types = class_df[,unique(test_name)]
  foreach(i=1:length(interaction_types)) %do% {
    test_type <- interaction_types[i]
    test_type_pvals <- class_df[test_name == test_type,Pvalue]
    rest_type_pvals <- class_df[test_name != test_type,Pvalue]
    wilcox.test(test_type_pvals,rest_type_pvals, alternative = "greater")$p.value
  }
} 

# compute the statistics for each band, interaction type 
#band_counts <- all_chrs_dt %>% 
#  group_by(test_name,upper) %>% 
#  summarise(sig_label = sum(sig_label), sig_notlabel = sum(sig_notlabel), not_sig_label = sum(not_sig_label), not_sig_notlabel = sum(as.numeric(not_sig_notlabel)))

### Just for fun, this is the data.table version
band_counts <- all_chrs_dt[, .(sig_label = sum(sig_label), sig_notlabel = sum(sig_notlabel), not_sig_label = sum(not_sig_label), not_sig_notlabel = sum(as.numeric(not_sig_notlabel))), by = .(test_name,upper)]

pvals <- band_counts[,list(test_name,upper,Pvalue=do_ft(sig_label,sig_notlabel,not_sig_label,not_sig_notlabel)), by=1:nrow(band_counts)]

# Transform the p-values to -log10(Pvalue), replacing the infinitesimal values (which become Inf) with 255
band_count_pvals <- pvals[,list(test_name, upper, Pvalue=-log10(Pvalue))]
infs_to_replace <- (band_count_pvals$Pvalue == Inf)
band_count_pvals$Pvalue[infs_to_replace] <- 255

# define a lookup table for binning the interactions by genomic distance
low = 10000
high = 2000000
bandwidth = 10000
n_bins = round(high / 50000)
breakpoints = seq(low, high, by = bandwidth)
breaks_per_group = round(length(breakpoints) / n_bins)

lookup = data.table( 
  upper = breakpoints, 
  region = unlist(lapply(1:n_bins,rep, times = breaks_per_group)))

# For a given partitioning of the genomic distance from 0 - 2mb, return a positive int that groups the values
setkey(band_count_pvals, upper)
setkey(lookup, upper)

band_class_pvals = band_count_pvals[lookup]

# Now merge the null and significant bands together 

# split by 100kb region, apply 10 tests, of '<label> greater than <rest>', combine results of all 10 tests, combine all region results, plot. 
options(warn = -1)
pval_enrichments <- band_class_pvals[,.(interaction_type=unique(test_name),enrichment=do_enr_test(.SD)), by = region]
options(warn = 0)

# log transform the p-value enrichments
pval_enrichments <- pval_enrichments[,.(interaction_type,region,pvalue=-log10(as.numeric(enrichment)))]

### Option 1: dump all those interaction_types that never have any pvalues which exceed the multiple testing correction level -log10(0.05 / 6)
pval_enrichments <- pval_enrichments[, significant := pvalue > -log10(0.05 / 6)]
# pval_enrichments <- pval_enrichments %>% group_by(interaction_type) %>% mutate(med_val = median(pvalue))
# no_loser_pvalues <- pval_enrichments[med_val > -log10(0.05 / 6),]

pval_line_plot <- ggplot(pval_enrichments, aes(x = region, y = pvalue, colour = factor(interaction_type), alpha = significant)) +
  geom_point(aes(size = pvalue)) + guides(size=FALSE) +
  geom_line(linetype="dashed", alpha = 0.30) +  
  ggtitle("Enrichment of Significant Interaction P-values for Genomic annotations \n One-vs-All Wilcoxon Rank-Sum Tests") + 
  xlab("Genomic Distance (50kb)") + 
  ylab("-log10(p-value)") + 
  geom_hline(yintercept = -log10(0.05 / 6), linetype = "longdash") +  
  scale_colour_manual(name = "Interaction Types", values = c("#1b9e77","#d95f02","#7570b3","#e7298a","#66a61e","#e6ab02"), drop = TRUE) + 
  scale_alpha_discrete(range = c(0.25, 1.0)) + guides(alpha = FALSE)



