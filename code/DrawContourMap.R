plotcontourmap <- function(feature.matrix,start,end,regions,tracks,maptype='Contourmap-Pvalue',alpha=0.01,beta=2){
    library(ggbio)
    library(Homo.sapiens)
    library(TxDb.Hsapiens.UCSC.hg19.knownGene)
    library(BSgenome.Hsapiens.UCSC.hg19)
    library(reshape2)
    library(Matrix)
    library(scales)
    library(data.table)
    
    k <- min(subjectHits(findOverlaps(GRanges(seqnames(regions[1]),IRanges(start=start,width=1)),regions)))
    m <- max(subjectHits(findOverlaps(GRanges(seqnames(regions[1]),IRanges(start=end,width=1)),regions)))
    n <- length(regions)
    
    if(maptype=='Contourmap-Counts'){
      x <- feature.matrix$counts
      censored <- x < beta
      x[censored] <- 1
      x <- log2(x)
      
      Hashtable <- data.table(bins=names(regions),index=1:length(regions))
      setkey(Hashtable,key='bins')
      
      A <- sparseMatrix(i=Hashtable[feature.matrix$binI][[2]],j=Hashtable[feature.matrix$binJ][[2]],x=x,dims=c(n,n))
      u <- A[k:m,k:m]
      u <- forceSymmetric(u)
      u <- as.matrix(u)
      row.names(u) <- start(resize(regions[k:m],fix = "center",width = 1))
      colnames(u) <- start(resize(regions[k:m],fix = "center",width = 1))
      v <- melt(u)
      p1 <- ggplot(v,aes(x=Var1,y=Var2,z=value))+ ylab("") + geom_tile(aes(fill=value)) + scale_fill_gradient2(low="white",mid="yellow", high="red")+
               theme(panel.background=element_rect(fill="grey50")) + theme(panel.grid=element_blank()) 
      #       p1 <- ggplot(v, aes(x=Var1, y=Var2, z=value))+ ylab("")+
      #              stat_contour(aes(color=..level..),binwidth=1,size=0.3)+
      #              scale_colour_gradient2(low="white",high="#00CC00") + theme(panel.background=element_rect(fill="grey50")) + theme(panel.grid=element_blank())  
    }
    if(maptype=='Contourmap-Pvalue'){
      y <- feature.matrix$pvals
      new.y <- y
      censored <- y==0
      new.y[censored] <- min(y[!censored])
      new.new.y <- new.y
      cen <- new.y > alpha
      new.new.y[cen] <- 1
      LY <- -log10(new.new.y)
      
      Hashtable <- data.table(bins=names(regions),index=1:length(regions))
      setkey(Hashtable,key='bins')
      
      A <- sparseMatrix(i=Hashtable[feature.matrix$binI][[2]],j=Hashtable[feature.matrix$binJ][[2]],x=LY,dims=c(n,n))
      u <- A[k:m,k:m]
      u <- forceSymmetric(u)
      u <- as.matrix(u)
      row.names(u) <- start(resize(regions[k:m],fix = "center",width = 1))
      colnames(u) <- start(resize(regions[k:m],fix = "center",width = 1))
      v <- melt(u)
      p1 <- ggplot(v, aes(x=Var1, y=Var2, z=value))+ ylab("")+
        stat_contour(aes(color=..level..),binwidth=1,size=0.3)+
        scale_colour_gradient2(low="white",high="red") + theme(panel.background=element_rect(fill="grey50")) + theme(panel.grid=element_blank()) 
    }
  
    ran <- start(resize(regions[k:m],fix = "center",width = 1))
    chr <- as.character(seqnames(regions[1]))
    txdb <- TxDb.Hsapiens.UCSC.hg19.knownGene
      
    p.ideo <- plotIdeogram(genome = "hg19",subchr=chr)
  
    wh <- GRanges(chr, IRanges(start=ran[1],end=ran[length(ran)]))
    p2 <- autoplot(txdb, which = wh,label=FALSE,stat="reduce",color="brown",fill="brown")
      
    G1 <- tracks$DNase[which(seqnames(tracks$DNase)==chr)]
    hits <- findOverlaps(G1,regions[k:m])
    len <- length(queryHits(hits))
    set.seed(101)
    idx <- sample(queryHits(hits),floor(len*0.8),replace=F)
    scores <- G1$score[idx]
    vec <- start(resize(G1[idx],fix="center",width=1))
    df <- data.table(pos=vec,score=scores)
    p3 <- ggplot(df,aes(x=pos,y=score)) + geom_area(colour="blue",fill="blue")
    
    G2 <- tracks$CTCF[which(seqnames(tracks$CTCF)==chr)]
    hits <- findOverlaps(G2,regions[k:m])
    len <- length(queryHits(hits))
    set.seed(101)
    idx <- sample(queryHits(hits),floor(len*0.8),replace=F)
    scores <- G2$score[idx]
    vec <- start(resize(G2[idx],fix="center",width=1))
    df <- data.table(pos=vec,score=scores)
    p4 <- ggplot(df,aes(x=pos,y=score)) + geom_area(colour="darkgreen",fill="green")
      
    G3 <- tracks$RNASeq[which(seqnames(tracks$RNASeq)==chr)]
    hits <- findOverlaps(G3,regions[k:m])
    len <- length(queryHits(hits))
    set.seed(101)
    idx <- sample(queryHits(hits),floor(len*0.8),replace=F)
    scores <- log10(G3$score[idx]+1)
    vec <- start(resize(G3[idx],fix="center",width=1))
    dt <- data.table(pos=vec,score=scores)
    p5 <- ggplot(dt,aes(x=pos,y=score)) + geom_area(colour="red",fill="red")
    
    G4 <- tracks$GM27ac[which(seqnames(tracks$GM27ac)==chr)]
    hits <- findOverlaps(G4,regions[k:m])
    len <- length(queryHits(hits))
    set.seed(101)
    idx <- sample(queryHits(hits),floor(len*0.8),replace=F)
    scores <- G4$score[idx]
    vec <- start(resize(G4[idx],fix="center",width=1))
    dt <- data.table(pos=vec,score=scores)
    p6 <- ggplot(dt,aes(x=pos,y=score)) + geom_area(colour="orange",fill="orange")
    
    G5 <- tracks$RAD21[which(seqnames(tracks$RAD21)==chr)]
    hits <- findOverlaps(G5,regions[k:m])
    len <- length(queryHits(hits))
    set.seed(101)
    idx <- sample(queryHits(hits),floor(len*0.8),replace=F)
    scores <- G5$score[idx]
    vec <- start(resize(G5[idx],fix="center",width=1))
    dt <- data.table(pos=vec,score=scores)
    p7 <- ggplot(dt,aes(x=pos,y=score)) + geom_area(colour="purple",fill="purple")
    
    G6 <- tracks$SMC3[which(seqnames(tracks$SMC3)==chr)]
    hits <- findOverlaps(G6,regions[k:m])
    len <- length(queryHits(hits))
    set.seed(101)
    idx <- sample(queryHits(hits),floor(len*0.8),replace=F)
    scores <- G6$score[idx]
    vec <- start(resize(G6[idx],fix="center",width=1))
    dt <- data.table(pos=vec,score=scores)
    p7 <- ggplot(dt,aes(x=pos,y=score)) + geom_area(colour="yellow",fill="yellow")

    tracks(Ideogram=p.ideo,Model=p2,RNASeq=p5,CTCF=p4,DNase1=p3,ContourMap = p1,scale.height = 2.5,xlim=wh,heights = c(0.8,0.7,1,1,1,1,1,1,2.5),padding=0.15)
}