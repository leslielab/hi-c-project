# We use a hurdle negative binomial regression model to remove systematic biases in Hi-C cis contact maps
# Decay in contact frequency with distance is modeled using a B-spline
# Mark A. Carty (mac449@cornell.edu)
# Last update: 5.13.2015
# Parallel processing (using the desired chunk size and number of cores)
# is implemented by Merve Sahin (ms3276@cornell.edu) on June 1, 2016.

#args[1] == file
#args[2] == min linear distance between locus pairs, default 0 bps
#args[3] == max linear distance between locus pairs, default 2 Mb
#args[4] == degree of freedom, default=6
#args[5] == sample size, default = 0.01
#args[6] == chunk size, try 500K or 1M for chr 1-2, 200K for chr10-11, and 100K for chr21-22, and so on.
#args[7] == number of cores used for parallel processing (chunk sizes above are decided using 5 cores)

# Loading required R and bioconductor packages to run the algorithm

library(pscl)
library(VGAM)
library(splines)
library(methods)
library(data.table)
# doParallel is used to prevent R leaving zombie processes behind
# since stopCluster() can be used with doParallel only!
library(doParallel)

args <- commandArgs(trailingOnly = TRUE)
options(warn=-1)

file <- args[1]
d.min <- as.integer(args[2])
d.max <- as.integer(args[3])
df <- as.integer(args[4])
ssize <- as.numeric(args[5])
chunksize<-as.numeric(args[6])
cores<-as.numeric(args[7])

# Declare functions

pzanegbin2 <- function(x,size,mu,p) dzanegbin(x = x,size = size,munb = mu,pobs0 = p) + 1 - pzanegbin(q = x,size = size,munb = mu,pobs0 = p)

GLM <- function(data,df,bdpts){
  mod <- hurdle(counts ~ len + gc + map + bs(D,df=df,Boundary.knots = bdpts)| len + gc + map + bs(D,df=df,Boundary.knots = bdpts),data=data,dist='negbin')
  return(mod)
}

func <- function(counts,size,mu,p){
  p_vec <- mapply(pzanegbin2,x=counts,size=size,mu=mu,p=p)
  return(p_vec)
}

# Main body of algorithm
set.seed(1010)

x <- readRDS(file) # reading in data
chr <- strsplit(as.character(x$binI[1]), "-")[[1]][1]
new.x <- x[D >= d.min & D <= d.max,]
zeroPairedBins <- new.x[counts==0,]
countPairedBins <- new.x[counts!=0,]

Intervals <- seq(min(new.x$D),to=max(new.x$D),by=4e4)
cls.counts <- findInterval(countPairedBins$D,Intervals,rightmost.closed = T)
cls.zeros <- findInterval(zeroPairedBins$D,Intervals,rightmost.closed = T)
bins.counts <- split(seq_len(nrow(countPairedBins)),cls.counts)
bins.zeros <- split(seq_len(nrow(zeroPairedBins)),cls.zeros)

idx.counts <- unlist(lapply(bins.counts,function(x){sample(x,floor(length(x)*ssize),replace = F)}))
idx.zeros <- unlist(lapply(bins.zeros,function(x){sample(x,floor(length(x)*ssize),replace = F)}))
dat <- rbind(countPairedBins[idx.counts,],zeroPairedBins[idx.zeros,])
bdpts <- range(new.x$D)
rm(cls.counts,cls.zeros,bins.counts,bins.zeros,idx.counts,idx.zeros,Intervals)
gc()
gc(reset=TRUE)

stime <- system.time({fit <- GLM(dat,df,bdpts)})[3]/60
show(sprintf('Time required to fit the model: %3.2f mins',stime))

# Register cores for parallel processing:
if(cores <= detectCores()){
cl<-makeCluster(cores)
registerDoParallel(cl)
}else if(cores > detectCores()){
# Stop if illegal number of cores specified!
stop("Number of cores specified is higher than the number of existing cores!")}

# Combine function for foreach:
comb.fnc<-function(...){
comb.mat<-apply(cbind(...), 1, function(x) unname(unlist(x)))
comb.list<-split(comb.mat, rep(1:ncol(comb.mat), each=nrow(comb.mat)))
names(comb.list)<-c("pvals", "mu")
return(comb.list)
}

stime <- system.time({

  fit.copy <- fit
  fit.copy$y <- sort(1:length(fit.copy$y)%%2)

# Split the countPairedBins to chunks
  partnum<-ceiling(nrow(new.x)/chunksize)
  splitseq<-rep(1:partnum, each=chunksize)[1:nrow(new.x)]
# split function will generate a list of chunks
  splitdata<-split(new.x, splitseq)  
# foreach will run every command for each chunk in parallel and then combine them with rbind!
  y<-foreach(x=splitdata, .packages = c('splines', 'VGAM','pscl'), .combine = 'comb.fnc') %dopar%
  {
    mu <- predict(fit,newdata = x, dispersion = fit$theta**(-1),type="count")
    phat <- predprob(fit.copy,newdata=x)[,1]
    pvals <- func(x$counts[!is.na(mu)], fit$theta, mu[!is.na(mu)], phat[!is.na(mu)])
    return(list("pvals"=pvals, "mu"=mu[!is.na(mu)]))
  }

})[3]/60
show(sprintf('Time required to estimate additional parameters : %3.2f mins',stime))

stopCluster(cl)

mu<-y$mu
pvals<-y$pvals
x.star <- new.x[!is.na(mu),]
x.star$pvals <- pvals
x.star$qvalue <- p.adjust(x.star$pvals,method='fdr')
x.star <- x.star[,list(binI=binI,binJ=binJ,counts=counts,D=D,len=len,gc=gc,map=map,pvalue=pvals,qvalue=qvalue)]
saveRDS(x.star, file = gsub('AllFeatures','Results',file))
saveRDS(fit, file = gsub('AllFeatures','Model-Fit',file))
rm(list=ls())
gc()
