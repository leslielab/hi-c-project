require(data.table)
require(stringr)

### apply labels to most significant contacts for HiC-DC

setwd('/Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/hurdle_hic/')

# load significant HiC-DC data
sig_files <- list.files(path = ".", pattern = "*is_sig.csv")
for (f in sig_files){
  chrom <- data.table(read.csv(file = f))
  if (!exists("all_chrs_dt")){
    all_chrs_dt <- data.table(chrom)
  } else {
    all_chrs_dt <- data.table(rbind(all_chrs_dt, chrom))
  }
}

# select top 8054 contacts, break down bins into chrI, startI, endI, chrJ, startJ, endJ
all_chrs_dt_select <- all_chrs_dt[D > 0,][order(qvalue)][1:8054]


all_chrs_dt_select[, c("chrI", "startI", "endI") := split_bin(binI), by=1:nrow(all_chrs_dt_select)]
all_chrs_dt_select[, c("chrJ", "startJ", "endJ") := split_bin(binJ), by=1:nrow(all_chrs_dt_select)]
all_chrs_dt <- all_chrs_dt_select[,.(chrI, as.numeric(startI), as.numeric(endI), chrJ, as.numeric(startJ), as.numeric(endJ), counts, D, qvalue)]
setnames(all_chrs_dt, c("V2", "V3", "V5", "V6") , c("startI", "endI", "startJ", "endJ"))
setkey(all_chrs_dt, chrI, startI, endI)

# apply the labels from combined Segway & ChromHMM
combined_labels <- data.table(read.delim(file = '../../revisions/bin_labels/wgEncodeAwgSegmentationCombinedGm12878.bed', header = FALSE))
colnames(combined_labels) <- c("chrom", "start", "end", "label", "score", "strand", "startagain", "endagain", "colour")
setkey(combined_labels, chrom, start, end)

labeled_binI <- foverlaps(all_chrs_dt, combined_labels, mult = "first")
setnames(labeled_binI, "label", "labelI")
labeled_binI <- labeled_binI[,.(chrI, startI, endI, chrJ, startJ, endJ, labelI, counts, D, qvalue)]
setkey(labeled_binI, chrJ, startJ, endJ)

hicdc_labeled_loops <- foverlaps(labeled_binI, combined_labels, mult = "first")
setnames(hicdc_labeled_loops, "label", "labelJ")
hicdc_labeled_loops <- hicdc_labeled_loops[,.(chrI, startI, endI, chrJ, startJ, endJ, labelI, labelJ, counts, D, qvalue)]
setwd('/Users/zamparol/projects/hi-c-project/data/revisions/loops')
saveRDS(hicdc_labeled_loops, file = "hiccups_loops_combined_labels.rds")

# apply the labels from CTCF ChIP-seq
setwd('/Users/zamparol/projects/hi-c-project/data/revisions/chip_peaks')
ctcf_files <- list.files(path = ".", pattern = "CTCF*")
for (f in ctcf_files){
  bf <- data.table(read.delim(file = f, header = FALSE))
  colnames(bf) <- c("chrom", "start", "end", "name", "score", "strand", "signal", "pvalue", "qvalue", "peak")
  if (!exists("ctcf_chip")){
    ctcf_chip <- data.table(bf)
  } else {
    ctcf_chip <- data.table(rbind(ctcf_chip, bf))
  }
}
ctcf_chip[,name := "CTCF"]

# Annotate loops with CTCF ChIP narrow peaks
setkey(ctcf_chip, chrom, start, end)
setkey(all_chrs_dt, chrI, startI, endI)

ctcf_binI <- foverlaps(all_chrs_dt, ctcf_chip, mult="first")
ctcf_binI <- ctcf_binI[,.(chrI, startI, endI, chrJ, startJ, endJ, D, counts, qvalue, i.qvalue, name)]
setkey(ctcf_binI, chrJ, startJ, endJ)
setnames(ctcf_binI, c("name", "i.qvalue"), c("labelI", "hicdc_qvalue"))
hicdc_ctcf_loops <- foverlaps(ctcf_binI, ctcf_chip, mult="first")
setnames(hicdc_ctcf_loops, c("name","i.qvalue"), c("labelJ", "hicdc_qvalue"))
hicdc_ctcf_loops <- hicdc_ctcf_loops[,.(chrI, startI, endI, chrJ, startJ, endJ, D, counts, qvalue, hicdc_qvalue, labelI, labelJ)]
saveRDS(hicdc_ctcf_loops, file = "ctcf_chip_annotated_hicdc_loops.rds")
