import pandas
import os
import sys

# get input files
infile_csv = sys.argv[1]
infile_gz = sys.argv[2]
indir_csv = sys.argv[3] # /Users/zamparol/projects/hi-c-project/data/csv/replicate
indir_gz = sys.argv[4] # /Users/zamparol/projects/hi-c-project/data/gzip/primary
outdir = sys.argv[5] # /Users/zamparol/projects/hi-c-project/data/csv/matched_replicate

# open matching replicate csv file, primary output file
replicate_csv = pandas.read_csv(os.path.join(indir_csv,infile_csv))
this_gzip_chr = pandas.read_csv(os.path.join(indir_gz,infile_gz), sep = '\t', compression = 'gzip')

# for all bins in replicate csv, get matching p-value from primary file
joined_df = pandas.merge(replicate_csv, this_gzip_chr, how='inner', on=["chr1","fragmentMid1","fragmentMid2"],
      left_index=False, right_index=False, sort=True, suffixes=('_r', '_p'))

# select only those fields we want to write out
joined_df_out = joined_df[["chr1","fragmentMid1","fragmentMid2","p-value_r","p-value_p","q-value_r","q-value_p","contactCount_r","contactCount_p"]]

# write out to output
joined_df_out.to_csv(os.path.join(outdir,infile_csv), index=False)
