#!/usr/bin/env python
'''
Created on Jan 15, 2015

@author: ferhat
'''

import sys
import os
import math
import errno
import argparse

# check if the path exists (protecting against race conditions in os.path.exists checks)
def make_sure_path_exists(path):
	try:
		os.makedirs(path)
	except OSError as exception:
		if exception.errno != errno.EEXIST:
			raise

USAGE = """USAGE: convertRaoToNormalizedContactCounts.py <indir> <cellLine> <resolution> <chromosome> <MAPQ> <outdir>

 This script is specifically for converting the Rao et al data (Cell 2014) from GEO entry GSE63525 
 under *_contact_matrices.tar.gz files to my regular 5-field contactCounts/ file format and the biases
 to my biasFile/ format of 3-fields third being the bias value. 

 <indir>: top-level directory where the downloaded and extracted Rao data resides
 <cellLine>: One of: CH12-LX GM12878_combined HMEC HUVEC K562 KBM7 NHEK (Note HeLa data is not released)
 <resolution>: One of: 1kb, 5kb, 10kb, 25kb, 50kb, 100kb, 250kb, 500kb, 1mb
 <chromosome>: Full name of the chromosome. E.g. chr8
 <MAPQ>: Either MAPQG0 or MAPQGE30. Use MAPQGE30 for reads with mapping quality above 30. 
 <outdir>: Output directory where the files will be written. Output file names will be:
	Normalized contact counts: afterICE/$resInt/<cellLine>.<chromosome>	
	Biases file: afterICE/$resInt/<cellLine>.<chromosome>.biases
	Raw contact counts: beforeICE/$resInt/<cellLine>.<chromosome>

"""

resDic={ "1kb":1000, "5kb":5000, "10kb":10000, "25kb":25000, "50kb":50000, "100kb":100000, "250kb":250000, "500kb":500000," 1mb":1000000}


def convertToMyFormat(indir, cellLine, resolution, chromosome, MAPQ, outdir):
	resInt=int(resDic[resolution])
	deepdir=indir+"/"+cellLine+"/"+resolution+"_resolution_intrachromosomal/"+chromosome+"/"+MAPQ
	KRnormFilename=deepdir+"/"+chromosome+"_"+resolution+".KRnorm"
	rawContactsFilename=deepdir+"/"+chromosome+"_"+resolution+".RAWobserved"

	make_sure_path_exists(deepdir)
	make_sure_path_exists(outdir+"/afterICE/"+str(resInt))
	make_sure_path_exists(outdir+"/beforeICE/"+str(resInt))
	
	outfileBias=open(outdir+"/afterICE/"+str(resInt)+"/"+cellLine+"."+chromosome+".biases",'w')
	outfileNormalized=open(outdir+"/afterICE/"+str(resInt)+"/"+cellLine+"."+chromosome,'w')
	outfileRaw=open(outdir+"/beforeICE/"+str(resInt)+"/"+cellLine+"."+chromosome,'w')

	KRnormFile=open(KRnormFilename,'r')
	mid=resInt/2
	biasDic={}
	for line in KRnormFile:
		bias=float(line.rstrip())
		biasDic[mid]=bias
		outfileBias.write(chromosome+"\t"+str(mid)+"\t"+repr(bias)+"\n")
		mid=mid+resInt
	#
	KRnormFile.close()

	rawContactsFile=open(rawContactsFilename,'r')
	for line in rawContactsFile:
		st1, st2, rawC=line.rstrip().split()
		mid1, mid2, rawC = int(st1)+resInt/2, int(st2)+resInt/2, float(rawC)
		mid1, mid2 = min(mid1,mid2), max(mid1,mid2)
		bias1=biasDic[mid1]
		bias2=biasDic[mid2]
		if math.isnan(bias1) or math.isnan(bias2):
			print "Skipping. Not a number (NaN) found for line: \t" + line.rstrip()
			continue
		#
		normC=1.0*rawC/(bias1*bias2)
		outfileRaw.write(chromosome+"\t"+str(mid1)+"\t"+chromosome+"\t"+str(mid2)+"\t"+str(int(rawC))+"\n")
		outfileNormalized.write(chromosome+"\t"+str(mid1)+"\t"+chromosome+"\t"+str(mid2)+"\t"+str(normC)+"\n")
	#
	rawContactsFile.close()

	outfileBias.close()
	outfileNormalized.close()
	outfileRaw.close()

	return


if __name__ == "__main__":
	''' <indir>: 
	 <cellLine>: 
	 <resolution>: 
	 <chromosome>: 
	 <MAPQ>:  
	 <outdir>:  Output file names will be:
		Normalized contact counts: afterICE/<resolution>/<cellLine>.<chromosome>	
		Biases file: afterICE/<resolution>/<cellLine>.<chromosome>.biases
		Raw contact counts: beforeICE/<resolution>/<cellLine>.<chromosome>
	'''	
	parser = argparse.ArgumentParser()
	parser.add_argument("-i", "--inputdir", help="top-level directory where the downloaded and extracted Rao data resides")
	parser.add_argument("-l", "--cellline", help="One of: CH12-LX GM12878_combined HMEC HUVEC K562 KBM7 NHEK (Note HeLa data is not released)")
	parser.add_argument("-r", "--resolution", help="One of: 1kb, 5kb, 10kb, 25kb, 50kb, 100kb, 250kb, 500kb, 1mb")
	parser.add_argument("-c", "--chromosome", help="Full name of the chromosome. E.g. chr8")
	parser.add_argument("-m", "--mapq", help="Either MAPQG0 or MAPQGE30. Use MAPQGE30 for reads with mapping quality above 30.")
	parser.add_argument("-o", "--outdir", help="Output directory where the files will be written.")
	args = parser.parse_args()
	
	convertToMyFormat(args.inputdir,args.cellline,args.resolution,args.chromosome,args.mapq,args.outdir)

