# coding: utf-8
import pandas
import sys
import re
import os

# get the input file, number of top values for each contact distance
### N.B: no longer using just the top values.  Now we're using all significant contacts.
infile = sys.argv[1]
top_values = int(sys.argv[2])
replicate = sys.argv[3]
indir = sys.argv[4]
outdir = sys.argv[5]

# extract chromosome name
grab_chr = re.compile('.*\.(chr[\d|X]+)\.spline')
m = re.match(grab_chr, infile)
chr_name = m.groups()[0]

# open the file, calculate distances
this_gzip_chr = pandas.read_csv(os.path.join(indir,infile), sep = '\t', compression = 'gzip')
this_gzip_chr["distance"] = this_gzip_chr["fragmentMid2"] - this_gzip_chr["fragmentMid1"]

# establish distance ranges
upper_bounds = [100000, 500000, 1000000, 2000000]
lower_bounds = [0, 100000, 500000, 1000000]

# write split by distance ranges
for low, high in zip(lower_bounds, upper_bounds):
    this_slice = this_gzip_chr[(this_gzip_chr['distance'] >= low) & (this_gzip_chr['distance'] < high) & (this_gzip_chr['q-value'] < 0.01)]
    #this_slice_out = this_slice.sort(['q-value'], ascending = True).head(top_values)
    slice_out_name = '_'.join([replicate,chr_name, str(low), "to", str(high)]) + ".csv"
    this_slice.to_csv(os.path.join(outdir,slice_out_name), index=False)