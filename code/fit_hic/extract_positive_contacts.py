import pandas
import os
import sys
import re


indir_fithic = sys.argv[1]  # /Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/fit_hic
outdir = sys.argv[2]        # /Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/fit_hic

# get input files
files = os.listdir(indir_fithic)
gz_files = [f for f in files if f.endswith('.gz')]

for f in gz_files:

    # find the fit hic chr
    my_chr = f.split('.')[1]
    
    # sometimes the data does not exist for matching chromosomes >>>>>>>>:|
    if not os.path.exists(os.path.join(indir_fithic,f)):
        continue
    
    fit_hic_chr = pandas.read_csv(os.path.join(indir_fithic,f), sep = '\t', compression = 'gzip')
    
    ### grab only positive contacts within (0,2000000], reformat to same bin style as HiC-DC
    pos_fit_hic = fit_hic_chr[fit_hic_chr['q-value'] < 0.01].copy(deep = True)
    pos_fit_hic['binIstart'] = pos_fit_hic['fragmentMid1'] - 2500
    pos_fit_hic['binIend'] = pos_fit_hic['fragmentMid1'] + 2500
    pos_fit_hic['binJstart'] = pos_fit_hic['fragmentMid2'] - 2500
    pos_fit_hic['binJend'] = pos_fit_hic['fragmentMid2'] + 2500
    pos_fit_hic['D'] = pos_fit_hic['fragmentMid2'] - pos_fit_hic['fragmentMid1']

    pos_fit_hic = pos_fit_hic[(pos_fit_hic['D'] > 0) & (pos_fit_hic['D'] < 2000001)]
    
    pos_fit_hic['binIstart'] = pos_fit_hic['binIstart'].astype(str)
    pos_fit_hic['binIend'] = pos_fit_hic['binIend'].astype(str)
    pos_fit_hic['binJstart'] = pos_fit_hic['binJstart'].astype(str)
    pos_fit_hic['binJend'] = pos_fit_hic['binJend'].astype(str)
    
    pos_fit_hic['binI'] = pos_fit_hic['chr1'] + "-" + pos_fit_hic['binIstart'] + "-" + pos_fit_hic['binIend']
    pos_fit_hic['binJ'] = pos_fit_hic['chr1'] + "-" + pos_fit_hic['binJstart'] + "-" + pos_fit_hic['binJend']
    
    ## select only those fields we want to write out
    ## n.b contactCount are fit hic counts, p-value, q-value are fit hic significances
    out_df = pos_fit_hic[["binI","binJ","contactCount","D","p-value","q-value"]]
    outname = "_".join([my_chr,"fit_hic_significant_contacts.csv"])
    
    ## write out to output
    out_df.to_csv(os.path.join(outdir,outname), index=False)
    