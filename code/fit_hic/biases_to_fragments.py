from __future__ import print_function
import sys 
import os

biases_input_dir='/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic/interactionCounts/afterICE/5000'
fragment_output_dir='/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic/fragmentMappability/beforeICE/5000'

# find all the .biases files in biases_input_dir
infiles = os.listdir(biases_input_dir)
infiles = [elem for elem in infiles if elem.endswith('.biases')]

for infile in infiles:        
    bias_file = open(os.path.join(biases_input_dir,infile), 'r')
    frag_file_name = os.path.splitext(os.path.join(fragment_output_dir,infile))[0]
    frag_file = open(frag_file_name, 'w')
    
    
    
    # parse each line in infile, write a corresponding fragment mappability to the output file.
    for line in bias_file:
        line = line.strip()
        parts = line.split()[:-1]
        endpt = int(parts[1])
        parts[1] = endpt - 2500
        parts.extend([endpt,1,1])
        print('\t'.join([str(item) for item in parts]), file = frag_file)
    
    # close infile, outfile
    bias_file.close()
    frag_file.close()
