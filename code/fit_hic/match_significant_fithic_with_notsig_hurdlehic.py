import pandas
import os
import sys
import re


indir_hurdle = sys.argv[1]  # /Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/hurdle_hic
indir_fithic = sys.argv[2]  # /Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/fit_hic
outdir = sys.argv[3]        # /Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/joined_data

# get input files
files = os.listdir(indir_hurdle)
get_chrs = re.compile(".+(chr[\\d|X]+)")
matches = [get_chrs.match(infile).groups()[0] for infile in files if infile.endswith('.rds')]
#matches = ['chr4','chr5']

for which_chr in matches:

    # find the fit hic, hurdle hic files to load
    hurdle_file = which_chr + 'not_sig.csv'
    fithic_file = 'GM12878_primary.' + which_chr + '.spline_pass1.res5000.significances.txt.gz'
    
    # sometimes the data does not exist for matching chromosomes >>>>>>>>:|
    if not os.path.exists(os.path.join(indir_hurdle,hurdle_file)):
        continue
    if not os.path.exists(os.path.join(indir_fithic,fithic_file)):
        continue
    
    # open matching replicate csv file, primary output file
    hurdle_chr = pandas.read_csv(os.path.join(indir_hurdle,hurdle_file))
    fit_hic_chr = pandas.read_csv(os.path.join(indir_fithic,fithic_file), sep = '\t', compression = 'gzip')
    
    ### grab only fit hic contacts that are significant, reformat into the same bin style as hurdle hic,
    # convert fit hic bin information into hurdle hic-like fit information
    #sig_fit_hic_chr = fit_hic_chr[fit_hic_chr['q-value'] < 0.01].copy(deep = True)
    #sig_fit_hic_chr['binIstart'] = sig_fit_hic_chr['fragmentMid1'] - 2500
    #sig_fit_hic_chr['binIend'] = sig_fit_hic_chr['fragmentMid1'] + 2500
    #sig_fit_hic_chr['binJstart'] = sig_fit_hic_chr['fragmentMid2'] - 2500
    #sig_fit_hic_chr['binJend'] = sig_fit_hic_chr['fragmentMid2'] + 2500
    
    #sig_fit_hic_chr['binIstart'] = sig_fit_hic_chr['binIstart'].astype(str)
    #sig_fit_hic_chr['binIend'] = sig_fit_hic_chr['binIend'].astype(str)
    #sig_fit_hic_chr['binJstart'] = sig_fit_hic_chr['binJstart'].astype(str)
    #sig_fit_hic_chr['binJend'] = sig_fit_hic_chr['binJend'].astype(str)
    
    #sig_fit_hic_chr['binI'] = sig_fit_hic_chr['chr1'] + "-" + sig_fit_hic_chr['binIstart'] + "-" + sig_fit_hic_chr['binIend']
    #sig_fit_hic_chr['binJ'] = sig_fit_hic_chr['chr1'] + "-" + sig_fit_hic_chr['binJstart'] + "-" + sig_fit_hic_chr['binJend']
    
    ## for all bins in replicate csv, get matching p-value from primary file
    #joined_df = pandas.merge(hurdle_chr, sig_fit_hic_chr, how='inner', on=["binI","binJ"], left_index=False, right_index=False, sort=True, suffixes=('_f', '_h'))
    
    ## select only those fields we want to write out
    ## n.b contactCount are fit hic counts, p-value, q-value are fit hic significances
    #fp_df = joined_df[["binI","binJ","contactCount","counts","D","p-value","q-value","len","gc","map","pvalue","qvalue"]]
    
    #outname = "_".join([which_chr,"fithic_sig_hurdle_not.csv"])
    
    ## write out to output
    #fp_df.to_csv(os.path.join(outdir,outname), index=False)
    
    
    ### now match the significant fit-hic, significant hurdle hic contacts
    sig_hurdle_file = which_chr + 'is_sig.csv'
    sig_hurdle_hic = pandas.read_csv(os.path.join(indir_hurdle,sig_hurdle_file))
    
    ## select the common significant bins from hurdle hic, fit hic
    #joined_df = pandas.merge(sig_hurdle_hic, sig_fit_hic_chr, how='inner', on=["binI","binJ"], left_index=False, right_index=False, sort=True, suffixes=('_f', '_h'))
    
    ## select only those fields we want to write out
    #tp_df = joined_df[["binI","binJ","contactCount","counts","D","p-value","q-value","len","gc","map","pvalue","qvalue"]]
    #outname = "_".join([which_chr,"both_sig.csv"])
    
    ## write out to output
    #tp_df.to_csv(os.path.join(outdir,outname), index=False)
    
    
    ### now match the significant hurdle hic contacts with their fit hi-c counterparts which are *not* significant
    not_sig_fit_hic_chr = fit_hic_chr[fit_hic_chr['q-value'] > 0.01].copy(deep = True)
    not_sig_fit_hic_chr['binIstart'] = not_sig_fit_hic_chr['fragmentMid1'] - 2500
    not_sig_fit_hic_chr['binIend'] = not_sig_fit_hic_chr['fragmentMid1'] + 2500
    not_sig_fit_hic_chr['binJstart'] = not_sig_fit_hic_chr['fragmentMid2'] - 2500
    not_sig_fit_hic_chr['binJend'] = not_sig_fit_hic_chr['fragmentMid2'] + 2500
    
    not_sig_fit_hic_chr['binIstart'] = not_sig_fit_hic_chr['binIstart'].astype(str)
    not_sig_fit_hic_chr['binIend'] = not_sig_fit_hic_chr['binIend'].astype(str)
    not_sig_fit_hic_chr['binJstart'] = not_sig_fit_hic_chr['binJstart'].astype(str)
    not_sig_fit_hic_chr['binJend'] = not_sig_fit_hic_chr['binJend'].astype(str)
    
    not_sig_fit_hic_chr['binI'] = not_sig_fit_hic_chr['chr1'] + "-" + not_sig_fit_hic_chr['binIstart'] + "-" + not_sig_fit_hic_chr['binIend']
    not_sig_fit_hic_chr['binJ'] = not_sig_fit_hic_chr['chr1'] + "-" + not_sig_fit_hic_chr['binJstart'] + "-" + not_sig_fit_hic_chr['binJend']    
    
    joined_df = pandas.merge(sig_hurdle_hic, not_sig_fit_hic_chr, how='inner', on=["binI","binJ"], left_index=False, right_index=False, sort=True, suffixes=('_f', '_h'))

    # select only those fields we want to write out
    only_sig_hurdle_df = joined_df[["binI","binJ","contactCount","counts","D","p-value","q-value","len","gc","map","pvalue","qvalue"]]
    outname = "_".join([which_chr,"hurdle_sig_fithic_not.csv"])
    
    # write out to output
    only_sig_hurdle_df.to_csv(os.path.join(outdir,outname), index=False)