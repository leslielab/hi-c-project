# coding: utf-8
import pandas
import sys
import re
import os

# get the input file, input & output dirs
outdir = '/Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/short_range_comparisons/fit_hi_c'

# extract chromosome name
grab_chr = re.compile('.*\.(chr[\d|X]+)\.spline')

# establish fit-hi-c prefix
replicate = 'primary'

infiles = [f for f in os.listdir('.') if f.endswith('.gz')]

for infile in infiles:
	m = re.match(grab_chr, infile)
	chr_name = m.groups()[0]

	# open the file, calculate distances
	this_gzip_chr = pandas.read_csv(infile, sep = '\t', compression = 'gzip')
	this_gzip_chr["distance"] = this_gzip_chr["fragmentMid2"] - this_gzip_chr["fragmentMid1"]

	# we want just the short contacts
	upper_bound = 100000
	lower_bound = 0

	# write split by distance ranges
    	this_slice = this_gzip_chr[(this_gzip_chr['distance'] >= lower_bound) & (this_gzip_chr['distance'] < upper_bound)]
    	slice_out_name = '_'.join([replicate,chr_name, str(lower_bound), "to", str(upper_bound)]) + ".csv"
    	this_slice.to_csv(os.path.join(outdir,slice_out_name), index=False)
