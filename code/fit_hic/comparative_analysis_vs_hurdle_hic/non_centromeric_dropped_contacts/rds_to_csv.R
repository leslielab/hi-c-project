require("data.table")
require("stringr")

### read all .rds files, take the set, convert contacts into CSV format for post-processing by python scripts (later to be visualized by circos), 

setwd('/Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/non_centromeric_nan_contacts')
chr_files <- list.files(path = '.', pattern = "[[:alnum:]|_]+.rds")

for (file in chr_files){
  chr_contacts <- readRDS(file)
  chr_name <- str_extract(file, "chr([\\d]+|X)")
  chr_contacts$chromosome <- chr_name
  chr_contacts_dt <- data.table(chr_contacts)
  
  if (!exists("all_chrs_contacts")){
    all_chrs_contacts <- chr_contacts_dt
  }
  else {
    all_chrs_contacts <- rbind(all_chrs_contacts,chr_contacts_dt)
  }
}

setkey(all_chrs_contacts, binI, binJ)
all_chrs_nodupes <- unique(all_chrs_contacts)

write.csv(all_chrs_nodupes,file = "/Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/non_centromeric_nan_contacts/lost_contacts.csv", row.names=FALSE)
