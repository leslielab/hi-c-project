from __future__ import print_function

import sys
import os
import csv

'''
# read .csv file sent out by R, parse and write out an .ssv file for consumption by circos

# an example of the output format required by circos for links
# start_chr pos_start pos_end end_chr pos_start pos_end [,sv list of properties]
# properties: color, thickness
#hs1 100 200 hs2 250 300 color=blue
#hs1 400 550 hs3 500 750 color=red,thickness=5p
#hs1 600 800 hs4 150 350 color=black
#...

'''

# read in the .csv file

input_dir = '/Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/non_centromeric_nan_contacts'
infile = os.path.join(input_dir, "lost_contacts.csv")

link_outfile = os.path.join(input_dir, "circos_links.ssv")

# open the .ssv files
links = open(link_outfile, 'w')

def replace_chrs(chrI,chrJ):
	return chrI.replace("chr","hs"), chrJ.replace("chr","hs")

# parsing helper for links
# return a readily printable string according to link spec in the docstring above 
def parse_for_links(line, links):
	props = "thickness="+line["counts"]
	chrI,startI,endI = line["binI"].split("-")
	chrJ,startJ,endJ = line["binJ"].split("-")
	chr_I, chr_J = replace_chrs(chrI,chrJ)
	outstring =  " ".join([chr_I, startI, endI, chr_J, startJ, endJ, props])
	print(outstring, file=links)

# parse each line, write parsed output to respective files
input_file = csv.DictReader(open(infile,'r'))
for line in input_file:
	parse_for_links(line, links)

# clean up		
links.close()
