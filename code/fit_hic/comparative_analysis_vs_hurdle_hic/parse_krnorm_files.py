from __future__ import print_function
import os
import sys



# take root directory as input, produce pandas DF as output
# crazy idea: why not just produce the plot with seaborn?

root_dir = sys.argv[1] # /Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/krnorm_files/GM12878_primary
mapq_dir = "MAPQG0"
outfile_name = "krnorms_stats.csv"

# open file, print header
outfile = open(os.path.join(root_dir,outfile_name),'w')
print(",".join(["resolution","chr","NaNs","Total"]),file=outfile)

# only go through the directories below root:
resolution_dirs = [d for d in os.listdir(root_dir) if d.endswith('intrachromosomal')]

for resolution_dir in resolution_dirs:
    this_resolution = resolution_dir.split("_")[0]
    for chr_dir in os.listdir(os.path.join(root_dir,resolution_dir)):
        this_chr = chr_dir
        chr_path = os.path.join(root_dir,resolution_dir,chr_dir,mapq_dir)
        chr_file = os.path.join(chr_path,os.listdir(chr_path)[0])
        chr_lines = open(chr_file,'r').readlines()
        num_nan =len([line for line in chr_lines if line.startswith("NaN")])
        num_bins = len(chr_lines)
        print(",".join([this_resolution,this_chr,str(num_nan),str(num_bins)]),file=outfile)
        
outfile.close()