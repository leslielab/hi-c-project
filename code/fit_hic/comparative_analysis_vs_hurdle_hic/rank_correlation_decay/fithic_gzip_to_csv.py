import pandas
import os
import sys
import re
import numpy as np

# get primary, replicate input files
indir_replicate = sys.argv[1] # /Users/zamparol/projects/hi-c-project/data/gzip/replicate
indir_primary = sys.argv[2] # /Users/zamparol/projects/hi-c-project/data/gzip/primary
outdir = sys.argv[3] # /Users/zamparol/projects/hi-c-project/data/ranked_contacts/fit_hic

# read all .gz files in both input dirs
primary_files = [f for f in os.listdir(indir_primary) if f.endswith('.gz')]
primary_files.sort()
replicate_files = [f for f in os.listdir(indir_replicate) if f.endswith('.gz')]
replicate_files.sort()

# tag each contact by distance
upper_bounds = [100000, 500000, 1000000, 2000000]
lower_bounds = [0, 100000, 500000, 1000000]

# set a limit for how many points we're looking to consider for each range 
limit = 5000

# get the chr
chr_name = re.compile('.*\.(chr[\d|X]+)\.')


for primary_file, rep_file in zip(primary_files,replicate_files):
    match = re.match(chr_name,primary_file)
    this_chr = match.groups()[0]
    
    this_primary_file = pandas.read_csv(os.path.join(indir_primary, primary_file), sep = '\t', compression = 'gzip')
    this_replicate_file = pandas.read_csv(os.path.join(indir_replicate, rep_file), sep = '\t', compression = 'gzip')
    this_primary_file['distance'] = this_primary_file['fragmentMid2'] - this_primary_file['fragmentMid1']
    this_replicate_file['distance'] = this_replicate_file['fragmentMid2'] - this_replicate_file['fragmentMid1']
    
    # tag each contact by distance, 
    for low, high in zip(lower_bounds, upper_bounds):
        distance_label = str(low)+"_to_"+str(high)
        primary_slice = this_primary_file[(this_primary_file['distance'] >= low) & (this_primary_file['distance'] < high) & (this_primary_file['q-value'] < 0.01)]
        replicate_slice = this_replicate_file[(this_replicate_file['distance'] >= low) & (this_replicate_file['distance'] < high) & (this_replicate_file['q-value'] < 0.01)]
        
        primary_slice['range'] = distance_label
        replicate_slice['range'] = distance_label
        
        # sort, rank each contact in this region
        primary_slice_sorted = primary_slice.sort_values(by=['q-value'], ascending = True)
        replicate_slice_sorted = replicate_slice.sort_values(by=['q-value'], ascending = True)
        primary_slice_sorted['rankPrimary'] = pandas.Series(data=np.arange(1,primary_slice_sorted.shape[0]+1,dtype=int), index=primary_slice_sorted.index)
        replicate_slice_sorted['rankReplicate'] = pandas.Series(data=np.arange(1,replicate_slice_sorted.shape[0]+1,dtype=int), index=replicate_slice_sorted.index)
        
        # merge primary with replicate, write out to .csv
        joined_df = pandas.merge(primary_slice_sorted, replicate_slice_sorted, how='inner', on=["chr1","fragmentMid1","fragmentMid2"], left_index=False, right_index=False, sort=True, suffixes=("_p","_r"))
        select_df = joined_df[joined_df.rankPrimary < limit]
        outslice_df = select_df[["chr1","fragmentMid1","fragmentMid2","distance_r","range_r","rankPrimary","rankReplicate"]]
        
        outname = "_".join([this_chr,"matched",distance_label,".csv"])
        outslice_df.to_csv(os.path.join(outdir,outname), index=False)