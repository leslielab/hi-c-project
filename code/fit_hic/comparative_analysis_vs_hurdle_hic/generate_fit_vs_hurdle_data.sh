#!/bin/bash

# extract matching significant data for hurdle hic (not sig) + fit hic (sig), && hurdle hic (sig) + fit hic (sig)

indir_hurdle='/Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/hurdle_hic'
indir_fithic='/Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/fit_hic'
outdir='/Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/joined_data'

python match_significant_fithic_with_notsig_hurdlehic.py $indir_hurdle $indir_fithic $outdir
