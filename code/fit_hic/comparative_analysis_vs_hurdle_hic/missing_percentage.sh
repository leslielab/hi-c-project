#!/bin/bash

set -o nounset
set -o pipefail
set -o errexit

# suppress pushd & popd output

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}


# get the dir where the data lies
in_dir=$1
curr_dir=`pwd`
pushd $in_dir

# how many bins are we missing?
declare -a kr_files=(`ls -1 *.KRnorm`)

for kr_file in "${kr_files[@]}" 
do
	nbins=(`wc -l $kr_file`)
	nans=(`grep -c NaN $kr_file`)
	q=$(bc -l <<< "100 * ($nans / $nbins)")
	print_q=`printf %.3f $q`
	echo "$kr_file has $nans NaN values for $nbins bins, which is $print_q percent"
done

popd

