### produce counts of number of significant interactions, at different FDR cutoffs, both including or not including D = 0 contacts
from __future__ import print_function
import pandas
import os
import sys

import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')

import seaborn as sns
sns.set(color_codes=True)

#beforeICE_indir = "/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic/primary/outputs/beforeICE/5000"
#afterICE_MAPQGE30_indir = "/Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data"

indir = sys.argv[1]
os.chdir(indir)

# go through all chromosomes in each output dir, assembling the table of number of interaction
infiles = [f for f in os.listdir('.') if f.endswith('.gz')]

# open .csv table output file in same dir
outfile = open("fit_hi_c_interactions.csv",'w')
print("chr","all interactions","significant interactions (FDR5%)","significant interactions (FDR1%)","interactions without D == 0 contacts", "significant interactions without D == 0 contacts (FDR5%)","significant interactions without D == 0 contacts (FDR1%)",sep=",",file=outfile)

for infile in infiles:
    
    # open the file, calculate distances
    this_gzip_chr = pandas.read_csv(infile, sep = '\t', compression = 'gzip')
    this_gzip_chr["distance"] = this_gzip_chr["fragmentMid2"] - this_gzip_chr["fragmentMid1"]

    # extract chromosome
    this_chr = this_gzip_chr.head()['chr1'][0]
    
    # count total number of interactions
    total_interactions = this_gzip_chr.shape[0]
    
    # count total number of interactions excluding D == 0 contacts
    this_slice = this_gzip_chr[this_gzip_chr['distance'] > 0]
    no_d0_interactions = this_slice.shape[0]
    
    # including D == 0 contacts, count total number of interactions significant at FDR 5%
    this_slice = this_gzip_chr[this_gzip_chr['q-value'] < 0.05]
    with_d0_fdr5p = this_slice.shape[0]
    
    # including D == 0 contacts, count total number of interactions significant at FDR 1%
    this_slice = this_gzip_chr[this_gzip_chr['q-value'] < 0.01]
    with_d0_fdr1p = this_slice.shape[0]
    
    # excluding D == 0 contacts, count total number of interactions significant at FDR 5%
    this_slice = this_gzip_chr[(this_gzip_chr['distance'] > 0) & (this_gzip_chr['q-value'] < 0.05)]
    exclude_d0_fdr5p = this_slice.shape[0]
    
    # excluding D == 0 contacts, count total number of interactions significant at FDR 1%
    this_slice = this_gzip_chr[(this_gzip_chr['distance'] > 0) & (this_gzip_chr['q-value'] < 0.01)]
    exclude_d0_fdr1p = this_slice.shape[0]
    
    
    # write to outfile
    print(this_chr, total_interactions, with_d0_fdr5p, with_d0_fdr1p, no_d0_interactions, exclude_d0_fdr5p, exclude_d0_fdr1p, sep=",", file=outfile)
    
    # produce q-value plot for this chromosome
    labelname = "Histogram of q-values for " + this_chr
    filename = this_chr + "_qvalues_hist.png"
    histplot = sns.distplot(this_gzip_chr['q-value'], kde=False, rug=False, hist=True, axlabel=labelname)
    myfig = histplot.get_figure()
    os.chdir('./qvalue_hists')
    myfig.savefig(filename, format='png')
    os.chdir('..')
    

outfile.close()    