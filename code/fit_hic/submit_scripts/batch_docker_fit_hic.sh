#!/bin/bash -ex

set -o nounset
set -o pipefail
set -o errexit 

# default args to Fit-Hi-C.  
export noOfBins=200
export distUpThres=2000000
export distLowThres=5000
export discBinsize=5000
export w=5000
export beforeAfter='beforeICE' # beforeICE or afterICE

export inF="/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic/primary/beforeICE/5000"
pushd "$inF"
declare -a libIDs=(`ls -1 *.chr*`)
popd

# submit the jobs
for libID in "${libIDs[@]}" 
do
    qsub -v CHR="$libID",NBINS="$noOfBins",UP="$distUpThres",DOWN="$distLowThres",SIZE="$discBinsize",W="$w",BA="$beforeAfter" submit_docker_fit_hic.sh
    sleep 2
		
done

