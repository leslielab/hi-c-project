#!/bin/bash -ex

set -o nounset
set -o pipefail
set -o errexit 

# default args to Fit-Hi-C.  
export noOfBins=200
export distUpThres=2000000
export distLowThres=5000
export discBinsize=5000
export w=5000
export MAPQ=MAPQGE30

# submit the beforeICE jobs
export beforeAfter='beforeICE' # beforeICE or afterICE

# get the chromosome names
#export inF="/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic_complete/MAPQG0/beforeICE/5000/"
#pushd "$inF"
#declare -a libIDs=(`ls -1 *.chr*`)
#popd

# docker cannot handle all jobs submitted so quickly, I need to re-run specific jobs
declare -a libIDs=(GM12878_primary.chr16 GM12878_primary.chrX)

# submit the jobs
for libID in "${libIDs[@]}" 
do
    qsub -v MAPQ="$MAPQ",CHR="$libID",NBINS="$noOfBins",UP="$distUpThres",DOWN="$distLowThres",SIZE="$discBinsize",W="$w",BA="$beforeAfter" submit_docker_fit_hic.sh
    sleep 10
		
done

# submit the afterICE jobs
beforeAfter='afterICE'
declare -a libIDs=(GM12878_primary.chr11)

# submit the jobs
for libID in "${libIDs[@]}"
do
    qsub -v MAPQ="$MAPQ",CHR="$libID",NBINS="$noOfBins",UP="$distUpThres",DOWN="$distLowThres",SIZE="$discBinsize",W="$w",BA="$beforeAfter" submit_docker_fit_hic.sh
    sleep 10

done
