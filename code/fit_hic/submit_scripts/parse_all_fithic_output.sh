#!/bin/bash -ex

set -o nounset
set -o pipefail
set -o errexit 

# common args to both primary and replicate data
export topvalues=5000

# args for primary replicate
export rep="primary"
export outdir="/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic/$rep/parsed_outputs"

# submit the jobs for the primary replicate
export inF="/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic/$rep/outputs/beforeICE/5000/"
pushd "$inF"
declare -a libIDs=(`ls -1 *.gz`)
popd

for libID in "${libIDs[@]}" 
do
    qsub -v CHR="$libID",TOP="$topvalues",REPLICATE="$rep",INDIR="$inF",OUTDIR="$outdir" submit_parse_fithic_output.sh
    sleep 1	
done

# args for the secondary replicate
export rep="replicate"
export outdir="/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic/$rep/parsed_outputs"

# submit the jobs for the secondary replicate
export inF="/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic/$rep/outputs/beforeICE/5000/"

pushd "$inF"
declare -a libIDs=(`ls -1 *.gz`)
popd

for libID in "${libIDs[@]}"
do
    qsub -v CHR="$libID",TOP="$topvalues",REPLICATE="$rep",INDIR="$inF",OUTDIR="$outdir" submit_parse_fithic_output.sh
    sleep 1

done

