#!/bin/bash -ex

set -o nounset
set -o pipefail
set -o errexit 

# $CHRSIG $CHRFP 
# common args for all processes
export binsdir="/cbio/cllab/nobackup/zamparol/alvaro_annotation_files/" 
export datadir="/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic/joined_data/"
export outputdir="/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic/annotated_joined_data/" 

# get the lists of significant contacts shared by both methods (bothsigs) and those only for fithic (fitsigs)
pushd "$datadir"
declare -a bothsigs=(`ls -1 *_both_sig.csv | sort`)
declare -a fitsigs=(`ls -1 *fithic_sig_hurdle_not.csv | sort`)
declare -a hursigs=(`ls -1 *hurdle_sig_fithic_not.csv | sort`)
popd

for index in "${!bothsigs[@]}"; 
do
  #echo "Element[$index]: ${bothsigs[$index]} ${fitsigs[$index]}"
  qsub -v CHRSIG="${bothsigs[$index]}",CHRFIT="${fitsigs[$index]}",CHRHUR="${hursigs[$index]}",BINSDIR="$binsdir",DATADIR="$datadir",OUTPUTDIR="$outputdir" submit_annotate_fixed_bin_data.sh
  sleep 1
done

