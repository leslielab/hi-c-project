#!/bin/bash
#  Batch script for single thread CPU job.
#
# walltime : maximum wall clock time (hh:mm:ss)
#PBS -l walltime=1:00:00,mem=32gb
#
# join stdout and stderr
#PBS -j oe
#
# spool output immediately
#PBS -k oe
#
# specify queue
#PBS -q batch
#
# nodes: number of nodes
#   ppn: number of processes per node
#PBS -l nodes=1:ppn=1
#
# export all my environment variables to the job
#PBS -V
#
# job name (default = name of script file)
#PBS -N parse_fithic_output
#
# specify email for notifications
#PBS -M zamparol@cbio.mskcc.org
#
# mail settings (one or more characters)
# n: do not send mail
# a: send mail if job is aborted
# b: send mail when job begins execution
# e: send mail when job terminates
#PBS -m n
#
# filename for standard output (default = <job_name>.o<job_id>)
# at end of job, it is in directory from which qsub was executed
# remove extra ## from the line below if you want to name your own file
##PBS -o myoutput

# Change to working directory used for job submission
cd $PBS_O_WORKDIR

# Get input ($CHR), number of values ($TOP), replicate ($REPLICATE), input dir ($INDIR), and output dir ($OUTDIR) from bash all submit script
python ../parse_fit_hic_output.py $CHR $TOP $REPLICATE $INDIR $OUTDIR
