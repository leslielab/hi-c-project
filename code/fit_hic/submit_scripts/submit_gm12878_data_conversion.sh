#!/bin/bash
#  Batch script for single thread CPU job.
#
# walltime : maximum wall clock time (hh:mm:ss)
#PBS -l walltime=14:00:00,mem=128gb
#
# join stdout and stderr
#PBS -j oe
#
# spool output immediately
#PBS -k oe
#
# specify queue
#PBS -q batch
#
# nodes: number of nodes
#   ppn: number of processes per node
#PBS -l nodes=1:ppn=8
#
# export all my environment variables to the job
#PBS -V
#
# job name (default = name of script file)
#PBS -N convert_gm12878_data_fit_hic
#
# specify email for notifications
#PBS -M zamparol@cbio.mskcc.org
#
# mail settings (one or more characters)
# n: do not send mail
# a: send mail if job is aborted
# b: send mail when job begins execution
# e: send mail when job terminates
#PBS -m n
#
# filename for standard output (default = <job_name>.o<job_id>)
# at end of job, it is in directory from which qsub was executed
# remove extra ## from the line below if you want to name your own file
##PBS -o myoutput

# Change to working directory used for job submission
cd $PBS_O_WORKDIR

# Get chr passed from all-submit bash script in $CHR
bash convert_all_GM12787_chromosomes.sh
