#!/bin/bash -ex

set -o nounset
set -o pipefail
set -o errexit 

# default args to Fit-Hi-C.  
export noOfBins=200
export distUpThres=2000000
export distLowThres=5000
export discBinsize=5000
export w=5000
export beforeAfter='afterICE' # beforeICE or afterICE


libID='GM12878_primary.chr22'
echo "CHR=$libID,NBINS=$noOfBins,UP=$distUpThres,DOWN=$distLowThres,SIZE=$discBinsize,W=$w,BA=$beforeAfter"
#qsub -v CHR="$libID",NBINS="$noOfBins",UP="$distUpThres",DOWN="$distLowThres",SIZE="$discBinsize",W="$w",BA="$beforeAfter" submit_docker_fit_hic.sh

