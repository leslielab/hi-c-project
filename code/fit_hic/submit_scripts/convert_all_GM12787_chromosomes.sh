#! /bin/bash

set -o nounset
set -o pipefail
set -o errexit

### input for all 
input_dir="/cbio/cllab/nobackup/zamparol/GM12878_combined/"
replicate="GM12878_primary"
resolution="5kb"
MAPQ="MAPQGE30"
output_dir="/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic_complete/$MAPQ"

#parallel --gnu echo "convertRaoToNormalizedContactCounts.py $input_dir $replicate $resolution chr{1} $MAPQ $output_dir" ::: {1..22} X

parallel --gnu python ../convertRaoToNormalizedContactCounts.py -i $input_dir -l $replicate -r $resolution -c chr{1} -m $MAPQ -o $output_dir ::: {1..22} X

