#!/bin/bash -ex

set -o nounset
set -o pipefail
set -o errexit 

# default args to Fit-Hi-C.  
export noOfBins=200
export distUpThres=2000000
export distLowThres=5000
export discBinsize=5000
export w=5000
export MAPQ=MAPQG0

# submit the beforeICE jobs
export beforeAfter='beforeICE' # beforeICE or afterICE

# get the chromosome names
#export inF="/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic_complete/MAPQG0/beforeICE/5000/"
#pushd "$inF"
#declare -a libIDs=(`ls -1 *.chr*`)
#popd

# Docker cannnot seem to pull everything needed at the same time, so re-do a specific list of stuff.

declare -a libIDs=(GM12878_primary.chr2 GM12878_primary.chr12)
# submit the jobs
for libID in "${libIDs[@]}" 
do
    qsub -v MAPQ="$MAPQ",CHR="$libID",NBINS="$noOfBins",UP="$distUpThres",DOWN="$distLowThres",SIZE="$discBinsize",W="$w",BA="$beforeAfter" submit_docker_fit_hic.sh
    sleep 10
		
done

# submit the afterICE jobs
beforeAfter='afterICE'
declare -a libIDs=(GM12878_primary.chr5 GM12878_primary.chr7 GM12878_primary.chr12 GM12878_primary.chrX)

# submit the jobs
for libID in "${libIDs[@]}"
do
    qsub -v MAPQ="$MAPQ",CHR="$libID",NBINS="$noOfBins",UP="$distUpThres",DOWN="$distLowThres",SIZE="$discBinsize",W="$w",BA="$beforeAfter" submit_docker_fit_hic.sh
    sleep 10

done
