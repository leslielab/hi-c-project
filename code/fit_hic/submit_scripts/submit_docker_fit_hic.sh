#!/bin/bash
#  Batch script for single thread CPU job.
#
# walltime : maximum wall clock time (hh:mm:ss)
#PBS -l walltime=5:00:00,mem=64gb
#
# join stdout and stderr
#PBS -j oe
#
# spool output immediately
#PBS -k oe
#
# specify queue
#PBS -q batch
#
# nodes: number of nodes
#   ppn: number of processes per node
#PBS -l nodes=1:ppn=1
#
# export all my environment variables to the job
#PBS -V
#
# job name (default = name of script file)
#PBS -N docker_fit_hic
#
# specify email for notifications
#PBS -M zamparol@cbio.mskcc.org
#
# mail settings (one or more characters)
# n: do not send mail
# a: send mail if job is aborted
# b: send mail when job begins execution
# e: send mail when job terminates
#PBS -m n
#
# filename for standard output (default = <job_name>.o<job_id>)
# at end of job, it is in directory from which qsub was executed
# remove extra ## from the line below if you want to name your own file
##PBS -o myoutput

# Change to working directory used for job submission
cd $PBS_O_WORKDIR

# environment vars originally from the step7 script
export PROJDIR='/data/GM12878_combined' # root of the data directory *** within the docker container.  This mapping is established in the submitjob script ***
export SRC='/src/fit_hic' # directory containing the fit-hi-c

# Where inputs are defined 
export inI="$PROJDIR/GM12878_fithic/primary/beforeICE/$W" # Ferhat: I always use the contact counts before ICE
export inF="$PROJDIR/GM12878_fithic/primary/beforeICE/$W" # Ferhat: only before ICE has these files
export inB="$PROJDIR/GM12878_fithic/primary/afterICE/$W" # Ferhat: I always use the bias files created after ICE

# create output directory on the HAL filesystem
mkdir -p "/cbio/cllab/nobackup/zamparol/GM12878_combined/GM12878_fithic/primary/outputs/$BA/$W"
# write output here, from the frame of reference of the docker job.
export outdir="$PROJDIR/GM12878_fithic/primary/outputs/$BA/$W"

# No idea what these mean, Ferhat knows.
export mappabilityThres=1
export noOfPasses=1
export residualFactor=-1

# pull the docker image from docker hub, and run the job
docker pull lzamparo/fithic:ready
if [[ $BA == "afterICE" ]];
then
   docker run -t -v /cbio/cllab/nobackup/zamparol/GM12878_combined/:/data/GM12878_combined/ -v /cbio/cllab/home/zamparol/projects/hi-c-project/code/fit_hic:/src/fit_hic -e PATH="/root/anaconda/bin:$PATH" -e PROJDIR -e SRC -e inF -e inI -e inB -e outdir -e BA -e CHR -e UP -e DOWN -e NBINS -e W -e mappabilityThres -e noOfPasses lzamparo/fithic:ready $SRC/fit-hi-c-fixedSize-withBiases.py -l $CHR -t "$inB/$CHR.biases" -f "$inF/$CHR" -i "$inI/$CHR" -L "$DOWN" -U "$UP" -b "$NBINS" -m "$mappabilityThres" -p "$noOfPasses" -r "$W" -o "$outdir/$CHR.out"
else
   docker run -t -v /cbio/cllab/nobackup/zamparol/GM12878_combined/:/data/GM12878_combined/ -v /cbio/cllab/home/zamparol/projects/hi-c-project/code/fit_hic:/src/fit_hic -e PATH="/root/anaconda/bin:$PATH" -e PROJDIR -e SRC -e inF -e inI -e inB -e outdir -e BA -e CHR -e UP -e DOWN -e NBINS -e W -e mappabilityThres -e noOfPasses lzamparo/fithic:ready $SRC/fit-hi-c-fixedSize-withBiases.py -l "$CHR" -f "$inF/$CHR" -i "$inI/$CHR" -L "$DOWN" -U "$UP" -b "$NBINS" -m "$mappabilityThres" -p "$noOfPasses" -r "$W" -o "$outdir/$CHR.out"
fi
