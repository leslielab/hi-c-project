import pandas
import os
import sys
import re


indir_hurdle = sys.argv[1]  # /Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/hurdle_hic
indir_fithic = sys.argv[2]  # /Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/fit_hic
outdir = sys.argv[3]        # /Users/zamparol/projects/hi-c-project/data/primary_rep_fixed_bin_data/common_negative_set_data

# get input files
files = os.listdir(indir_hurdle)
get_chrs = re.compile("(chr[\\d|X]+).+")
matches = [get_chrs.match(infile).groups()[0] for infile in files if infile.endswith('is_sig.csv')]
#matches = ['chr2']

for which_chr in matches:

    # find the fit hic, hurdle hic files to load
    hurdle_file = which_chr + 'not_sig.csv'
    fithic_file = 'GM12878_primary.' + which_chr + '.spline_pass1.res5000.significances.txt.gz'
    
    # sometimes the data does not exist for matching chromosomes >>>>>>>>:|
    if not os.path.exists(os.path.join(indir_hurdle,hurdle_file)):
        continue
    if not os.path.exists(os.path.join(indir_fithic,fithic_file)):
        continue
    
    # open matching replicate csv file, primary output file
    hurdle_chr = pandas.read_csv(os.path.join(indir_hurdle,hurdle_file))
    fit_hic_chr = pandas.read_csv(os.path.join(indir_fithic,fithic_file), sep = '\t', compression = 'gzip')
    
    ### grab only negative contacts, reformat to same bin style as HiC-DC
    # convert fit hic bin information into hurdle hic-like fit information
    neg_fit_hic = fit_hic_chr[fit_hic_chr['q-value'] > 0.01].copy(deep = True)
    neg_fit_hic['binIstart'] = neg_fit_hic['fragmentMid1'] - 2500
    neg_fit_hic['binIend'] = neg_fit_hic['fragmentMid1'] + 2500
    neg_fit_hic['binJstart'] = neg_fit_hic['fragmentMid2'] - 2500
    neg_fit_hic['binJend'] = neg_fit_hic['fragmentMid2'] + 2500
    
    neg_fit_hic['binIstart'] = neg_fit_hic['binIstart'].astype(str)
    neg_fit_hic['binIend'] = neg_fit_hic['binIend'].astype(str)
    neg_fit_hic['binJstart'] = neg_fit_hic['binJstart'].astype(str)
    neg_fit_hic['binJend'] = neg_fit_hic['binJend'].astype(str)
    
    neg_fit_hic['binI'] = neg_fit_hic['chr1'] + "-" + neg_fit_hic['binIstart'] + "-" + neg_fit_hic['binIend']
    neg_fit_hic['binJ'] = neg_fit_hic['chr1'] + "-" + neg_fit_hic['binJstart'] + "-" + neg_fit_hic['binJend']
    
    # for all bins in replicate csv, get matching p-value from primary file
    joined_df = pandas.merge(hurdle_chr, neg_fit_hic, how='inner', on=["binI","binJ"], left_index=False, right_index=False, sort=True, suffixes=('_f','_h'))

    
    ## select only those fields we want to write out
    ## n.b contactCount are fit hic counts, p-value, q-value are fit hic significances
    joint_negative_out_df = joined_df[["binI","binJ","contactCount","counts","D","p-value","q-value","len","gc","map","pvalue","qvalue"]]
    outname = "_".join([which_chr,"agreed_not_significant_contacts.csv"])
    
    ## write out to output
    joint_negative_out_df.to_csv(os.path.join(outdir,outname), index=False)
    
